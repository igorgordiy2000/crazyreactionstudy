-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -548.5449706065
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000139323 
   Number of Beta  Electrons                 16.0000139323 
   Total number of  Electrons                32.0000278647 
   Exchange energy                          -41.3898711205 
   Correlation energy                        -1.1623356410 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.5522067615 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5449706065 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.2652    16.0000     0.7348     3.2757     3.2757    -0.0000
  1   0     8.3674     8.0000    -0.3674     1.9051     1.9051     0.0000
  2   0     8.3674     8.0000    -0.3674     1.9051     1.9051    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.637841
                0            16               2            8                1.637846
                1             8               2            8                0.267228
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0004017368
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -548.5690463406
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 16.0000103154 
   Number of Beta  Electrons                 16.0000103154 
   Total number of  Electrons                32.0000206307 
   Exchange energy                          -41.4799305020 
   Correlation energy                        -1.1673795421 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.6473100441 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5690463406 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0004039460
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -548.5740029544
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 16.0000017097 
   Number of Beta  Electrons                 16.0000017097 
   Total number of  Electrons                32.0000034194 
   Exchange energy                          -41.6201404709 
   Correlation energy                        -1.1739761536 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7941166244 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5740029544 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0004062543
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -548.5767399787
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 16.0000030555 
   Number of Beta  Electrons                 16.0000030555 
   Total number of  Electrons                32.0000061111 
   Exchange energy                          -41.5577390859 
   Correlation energy                        -1.1712128031 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7289518889 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5767399787 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0004052106
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -548.5768181337
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 16.0000024630 
   Number of Beta  Electrons                 16.0000024630 
   Total number of  Electrons                32.0000049260 
   Exchange energy                          -41.5666418701 
   Correlation energy                        -1.1716250864 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7382669566 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5768181337 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0004053398
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -548.5768156900
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 16.0000023539 
   Number of Beta  Electrons                 16.0000023539 
   Total number of  Electrons                32.0000047077 
   Exchange energy                          -41.5677819299 
   Correlation energy                        -1.1716780269 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7394599568 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5768156900 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0004053520
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -548.5768156464
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 16.0000023463 
   Number of Beta  Electrons                 16.0000023463 
   Total number of  Electrons                32.0000046927 
   Exchange energy                          -41.5677464102 
   Correlation energy                        -1.1716763492 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7394227594 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5768156464 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 7
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.2531    16.0000     0.7469     3.2598     3.2598    -0.0000
  1   0     8.3734     8.0000    -0.3734     1.7658     1.7658    -0.0000
  2   0     8.3734     8.0000    -0.3734     1.7658     1.7658    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.629904
                0            16               2            8                1.629902
                1             8               2            8                0.135940
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0004053504
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 7
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14285-qCLZ/so2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.6858500678
        Electronic Contribution:
                  0    
      0       0.000000
      1       0.000004
      2       0.656891
        Nuclear Contribution:
                  0    
      0       0.000000
      1      -0.000000
      2       0.006360
        Total Dipole moment:
                  0    
      0       0.000000
      1       0.000004
      2       0.663251
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 7
   prop. index: 1
Normal modes:
Number of Rows: 9 Number of Columns: 9
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8    
      0      -0.000000  -0.000000  -0.000000
      1       0.000001  -0.000007  -0.518437
      2      -0.399492   0.356114  -0.000006
      3       0.000000  -0.000000   0.000000
      4       0.510672   0.556707   0.518142
      5       0.399270  -0.355920  -0.311657
      6       0.000000   0.000000  -0.000000
      7      -0.510674  -0.556694   0.518155
      8       0.399269  -0.355912   0.311669
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 7
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         63.9619004136
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -548.5711990589
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0002604423
        Number of frequencies          :     9      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     497.121725
      7     1122.230181
      8     1296.767962
        Zero Point Energy (Hartree)    :          0.0066434099
        Inner Energy (Hartree)         :       -548.5614626639
        Enthalpy (Hartree)             :       -548.5605184548
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0096884778
        Vibrational entropy            :          0.0003568299
        Translational entropy          :          0.0096884778
        Entropy                        :          0.0282836428
        Gibbs Energy (Hartree)         :       -548.5888020976
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 S      0.000000000000   -0.000000071000    0.464422083000
               1 O      0.000000000000    1.351921805000   -0.413795063000
               2 O      0.000000000000   -1.351921734000   -0.413795021000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 S      0.000000000000    0.000000207711    0.427954649094
               1 O      0.000000000000    1.295196794009   -0.395561256076
               2 O      0.000000000000   -1.295197001720   -0.395561394018
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     3 
    Coordinates:
               0 S      0.000000000000   -0.000000160289    0.373251212549
               1 O      0.000000000000    1.229660283135   -0.368209651705
               2 O      0.000000000000   -1.229660122847   -0.368209561844
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     4 
    Coordinates:
               0 S      0.000000000000    0.000000025399    0.389127096370
               1 O      0.000000000000    1.263100104128   -0.376147537302
               2 O      0.000000000000   -1.263100129527   -0.376147560067
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     5 
    Coordinates:
               0 S      0.000000000000    0.000002996835    0.384868710897
               1 O      0.000000000000    1.259728051602   -0.374017444380
               2 O      0.000000000000   -1.259731048437   -0.374019267517
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     6 
    Coordinates:
               0 S      0.000000000000   -0.000000112914    0.384075027627
               1 O      0.000000000000    1.259512930293   -0.373621538174
               2 O      0.000000000000   -1.259512817378   -0.373621490453
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     7 
    Coordinates:
               0 S      0.000000000000   -0.000000226165    0.384029564199
               1 O      0.000000000000    1.259580085361   -0.373598840529
               2 O      0.000000000000   -1.259579859196   -0.373598724670
