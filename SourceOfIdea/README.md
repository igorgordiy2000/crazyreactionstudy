# Source of the idea

The idea of this project has appeared from the [following post in VK social network](https://vk.com/wall-196254731_3088).
There, a [Tiemann Amidoxime-Urea Rearrangement reaction](https://doi.org/10.1002/9780470638859.conrr622) was shown.


So, an idea appeared to recompute this reaction using quantum-chemistry in a crowd-making method, to get the job done, not wasting a lot of time.
Here, we also store the screenshot of that message.
