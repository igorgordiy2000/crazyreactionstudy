                  a.u.                     D                a.u.          kcal/mol         kcal/mol      kcal/mol
Reagent.xyz:      Energy -1158.5826771067  Dipole 2.970361  ZPE 0.198167  G(298.15) 91.38  Edisp -21.24  Gsolv(benzene) -10.53
TS1.xyz:          Energy -1158.5624670772  Dipole 3.474106  ZPE 0.197838  G(298.15) 95.21  Edisp -23.21  Gsolv(benzene) -9.64
Intermediate.xyz: Energy -1158.5960946141  Dipole 2.585484  ZPE 0.199944  G(298.15) 97.42  Edisp -23.35  Gsolv(benzene) -10.03
TS2.xyz:          Energy -1158.5613362002  Dipole 3.465901  ZPE 0.196933  G(298.15) 94.64  Edisp -23.09  Gsolv(benzene) -10.51
Product.xyz:      Energy -1158.6707818192  Dipole 2.614013  ZPE 0.198930  G(298.15) 91.66  Edisp -21.90  Gsolv(benzene) -9.48

Edisp: D3BJ on optimized geometry by Grimme's dft-d3 program (https://www.chemiebn.uni-bonn.de/pctc/mulliken-center/software/dft-d3/dft-d3)
Gsolv: by Minenkov's SPT-V method for optimized geometry (https://doi.org/10.1021/acs.jctc.3c00410)
