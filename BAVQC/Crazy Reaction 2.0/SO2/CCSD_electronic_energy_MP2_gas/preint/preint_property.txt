-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -944.6083525216
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1336     6.0000    -0.1336     3.7913     3.7913     0.0000
  1   0     6.1654     6.0000    -0.1654     3.8479     3.8479    -0.0000
  2   0     6.1482     6.0000    -0.1482     3.8295     3.8295     0.0000
  3   0     5.9138     6.0000     0.0862     3.7660     3.7660     0.0000
  4   0     6.1575     6.0000    -0.1575     3.8234     3.8234    -0.0000
  5   0     6.1304     6.0000    -0.1304     3.7776     3.7776     0.0000
  6   0     0.8327     1.0000     0.1673     0.9804     0.9804    -0.0000
  7   0     6.0117     6.0000    -0.0117     3.9231     3.9231     0.0000
  8   0     0.8368     1.0000     0.1632     0.9750     0.9750     0.0000
  9   0     0.8342     1.0000     0.1658     0.9758     0.9758     0.0000
 10   0     0.8150     1.0000     0.1850     1.0007     1.0007     0.0000
 11   0     0.8342     1.0000     0.1658     0.9743     0.9743    -0.0000
 12   0     6.6992     7.0000     0.3008     4.0002     4.0002    -0.0000
 13   0     8.4620     8.0000    -0.4620     1.7180     1.7180    -0.0000
 14   0     8.4967     8.0000    -0.4967     2.0163     2.0163    -0.0000
 15   0    15.0746    16.0000     0.9254     3.8931     3.8931    -0.0000
 16   0     8.4539     8.0000    -0.4539     2.0533     2.0533    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.391693
                0             6               2            6                1.371634
                0             6               8            1                0.974063
                1             6               3            6                1.322276
                1             6               6            1                0.972422
                2             6               5            6                1.365757
                2             6               9            1                0.974668
                3             6               4            6                1.337121
                3             6               7            6                1.065188
                4             6               5            6                1.381245
                4             6              10            1                0.950209
                5             6              11            1                0.973714
                7             6              12            7                2.574506
                7             6              13            8                0.255610
               12             7              13            8                1.362512
               14             8              15           16                1.881190
               14             8              16            8                0.101203
               15            16              16            8                1.943006
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6083525189
        Total Correlation Energy:                                          -2.3095858102
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1145542039
        Total MDCI Energy:                                               -946.9179383291
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -944.6576094582
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 17
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0980     6.0000    -0.0980     3.7927     3.7927     0.0000
  1   0     6.1749     6.0000    -0.1749     3.8692     3.8692    -0.0000
  2   0     6.1489     6.0000    -0.1489     3.8533     3.8533    -0.0000
  3   0     5.9513     6.0000     0.0487     3.7542     3.7542     0.0000
  4   0     6.1700     6.0000    -0.1700     3.8458     3.8458    -0.0000
  5   0     6.0985     6.0000    -0.0985     3.7722     3.7722    -0.0000
  6   0     0.8395     1.0000     0.1605     0.9922     0.9922     0.0000
  7   0     5.8480     6.0000     0.1520     3.8118     3.8118    -0.0000
  8   0     0.8450     1.0000     0.1550     0.9865     0.9865    -0.0000
  9   0     0.8343     1.0000     0.1657     0.9840     0.9840    -0.0000
 10   0     0.8208     1.0000     0.1792     1.0131     1.0131    -0.0000
 11   0     0.8421     1.0000     0.1579     0.9844     0.9844    -0.0000
 12   0     6.8160     7.0000     0.1840     3.9396     3.9396    -0.0000
 13   0     8.4780     8.0000    -0.4780     1.7297     1.7297    -0.0000
 14   0     8.5588     8.0000    -0.5588     1.9550     1.9550    -0.0000
 15   0    14.9705    16.0000     1.0295     3.8434     3.8434     0.0000
 16   0     8.5054     8.0000    -0.5054     1.9939     1.9939    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.417626
                0             6               2            6                1.388881
                0             6               8            1                0.985799
                1             6               3            6                1.301317
                1             6               6            1                0.984083
                2             6               5            6                1.377448
                2             6               9            1                0.983538
                3             6               4            6                1.325700
                3             6               7            6                1.128376
                4             6               5            6                1.400577
                4             6              10            1                0.960190
                5             6              11            1                0.984075
                7             6              12            7                2.492450
                7             6              13            8                0.216227
               12             7              13            8                1.394792
               14             8              15           16                1.856498
               15            16              16            8                1.912411
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6576094603
        Total Correlation Energy:                                          -2.4500117410
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1251965263
        Total MDCI Energy:                                               -947.1076212013
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -944.6083525189 
       SCF energy with basis  cc-pVQZ :   -944.6576094603 
       CBS SCF Energy                    :   -944.6724507700 
       Correlation energy with basis  cc-pVTZ :   -944.6083525189 
       Correlation energy with basis  cc-pVQZ :   -944.6576094603 
       CBS Correlation Energy            :     -2.5499794844 
       CBS Total Energy                  :   -947.2224302544 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             434
     number of aux C basis functions:       1885
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14373-t1q1/preint.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.2051439305
        Electronic Contribution:
                  0    
      0      10.492017
      1       8.370238
      2       0.812554
        Nuclear Contribution:
                  0    
      0     -12.093527
      1      -9.524860
      2      -0.268883
        Total Dipole moment:
                  0    
      0      -1.601510
      1      -1.154623
      2       0.543671
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.555405869000   -0.892098023000   -0.434508706000
               1 C     -1.803161539000    0.265705720000   -0.600272754000
               2 C     -1.981559459000   -2.032449961000    0.124680751000
               3 C     -0.460705371000    0.278113892000   -0.199700179000
               4 C      0.122443457000   -0.865622533000    0.365178100000
               5 C     -0.645144294000   -2.013929753000    0.521730593000
               6 H     -2.241469610000    1.153735403000   -1.033413773000
               7 C      0.330049404000    1.452320062000   -0.359234328000
               8 H     -3.591599755000   -0.902549820000   -0.742892890000
               9 H     -2.571801708000   -2.929278055000    0.250714958000
              10 H      1.158364976000   -0.833869676000    0.672026477000
              11 H     -0.196770783000   -2.896547556000    0.956380227000
              12 N      1.068546819000    2.362154236000   -0.468178093000
              13 O      1.848873732000    3.283336709000   -0.552907376000
              14 O      2.912308521000    0.994672907000    1.206005340000
              15 S      3.898855517000    1.972156335000    0.728269047000
              16 O      4.708175961000    1.604150110000   -0.433877398000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.555405869000   -0.892098023000   -0.434508706000
               1 C     -1.803161539000    0.265705720000   -0.600272754000
               2 C     -1.981559459000   -2.032449961000    0.124680751000
               3 C     -0.460705371000    0.278113892000   -0.199700179000
               4 C      0.122443457000   -0.865622533000    0.365178100000
               5 C     -0.645144294000   -2.013929753000    0.521730593000
               6 H     -2.241469610000    1.153735403000   -1.033413773000
               7 C      0.330049404000    1.452320062000   -0.359234328000
               8 H     -3.591599755000   -0.902549820000   -0.742892890000
               9 H     -2.571801708000   -2.929278055000    0.250714958000
              10 H      1.158364976000   -0.833869676000    0.672026477000
              11 H     -0.196770783000   -2.896547556000    0.956380227000
              12 N      1.068546819000    2.362154236000   -0.468178093000
              13 O      1.848873732000    3.283336709000   -0.552907376000
              14 O      2.912308521000    0.994672907000    1.206005340000
              15 S      3.898855517000    1.972156335000    0.728269047000
              16 O      4.708175961000    1.604150110000   -0.433877398000
